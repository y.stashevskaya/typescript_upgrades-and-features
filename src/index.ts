class MakeRange { // no need to explicitly implement the interface
    private _first: number;
    private _last: number
   
    constructor(first: number, last: number) {
        this._first = first;
        this._last = last;
    }

    [Symbol.iterator]() {
        return this;
    }

    next() {
        if (this._first >= this._last) {
            return {value: this._first--, done: false}
        } else {
            return {value: undefined, done: true}
        }
   }
}

/*
* Task 1: Countdown iterator
*/
function countdown(a: number, b: number): Iterable<number> {
    return new MakeRange(a, b);
}

/*
* Task 2: Occurrences in array
*/
function countOccurrences<T>(arr: Array<T>, val: T): number {
    var result = 0;
    arr.forEach(item => {
        if (item === val) {
            result += 1;
        }
    })

    return result;
}


export {countdown, countOccurrences}
